import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RestService } from '../rest-services';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  loginForm = {nickName: '', password: ''};

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private restService: RestService
  ) {}

  ngOnInit() {
  }

  onSubmit() {
    this.restService.login(this.loginForm).subscribe(data => {
      console.log(data);
        location.href = '/home/' + data.userId;
      });
  }


}
