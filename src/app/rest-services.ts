
import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RestService {
    private apiUrl = 'http://localhost:8080/liste-courses-api/';
    private resourcesPath = {
        CATEGORIES: this.apiUrl + 'categories',
        USERS: this.apiUrl + 'users'
    };

    constructor(private http: Http) {}

    public getCategories() {
        return this.http.get(this.resourcesPath.CATEGORIES)
                    .map((res: Response) => res.json());
    }

    public login(loginForm: any) {
        return this.http.post(this.resourcesPath.USERS + '/login', loginForm)
                    .map((res: Response) => res.json());
    }

    public getUserShoppingList(userId: number) {
        return this.http.get(this.resourcesPath.USERS + '/' + userId + '/shoppinglists')
        .map((res: Response) => res.json());
    }
}
