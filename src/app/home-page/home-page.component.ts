import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest-services';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  private userId: number;
  private shoppingLists;

  constructor(
    private restService: RestService,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.userId = params['userId'];
      console.log(this.userId);
    });

    this.restService.getUserShoppingList(this.userId).subscribe(data => {
      this.shoppingLists = data;
    });
  }

}
